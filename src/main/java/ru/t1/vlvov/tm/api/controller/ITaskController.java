package ru.t1.vlvov.tm.api.controller;

public interface ITaskController {

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void createTask();

    void clearTasks();

    void showTasks();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByProjectId();

    void removeTaskById();

    void removeTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

}
