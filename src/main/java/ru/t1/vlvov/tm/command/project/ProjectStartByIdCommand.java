package ru.t1.vlvov.tm.command.project;

import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "Start project by Id.";

    private final String NAME = "project-start-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(id, Status.IN_PROGRESS);
    }

}