package ru.t1.vlvov.tm.command.task;

import ru.t1.vlvov.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Create task.";

    private final String NAME = "task-create";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getTaskService().create(name, description);
    }

}