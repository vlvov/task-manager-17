package ru.t1.vlvov.tm.command.system;

import ru.t1.vlvov.tm.api.model.ICommand;
import ru.t1.vlvov.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandListCommand extends AbstractSystemCommand {

    private final String ARGUMENT = "-cmd";

    private final String DESCRIPTION = "Show commands.";

    private final String NAME = "commands";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        System.out.println("[COMMANDS]");
        for (final ICommand command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
